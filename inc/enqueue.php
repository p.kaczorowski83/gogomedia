<?php
/**
 * ===============================
 * LOADING FILES - CSS/JS
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
function cbk_scripts_loader() {
	$theme_version = wp_get_theme()->get( 'Version' );
	// wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', false, $theme_version, 'all' );
	wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/css/main.css', array(), time()); // main.scss: Compiled Framework source + custom styles
	wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/assets/css/jquery.fancybox.min.css', array(), time());

	if ( is_rtl() ) {
		wp_enqueue_style( 'rtl', get_template_directory_uri() . '/assets/css/rtl.css', false, $theme_version, 'all' );
	}

	wp_enqueue_script( 'mainjs', get_template_directory_uri() . '/assets/js/main.bundle.js', array( 'jquery' ), time(), 'all');
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_dequeue_style( 'wp-block-library' );
	wp_dequeue_style( 'wp-block-library-theme' );
	wp_dequeue_style( 'wc-block-style' );
	
}
add_action( 'wp_enqueue_scripts', 'cbk_scripts_loader' );