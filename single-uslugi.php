<?php
/**
 * ===============================
 * SINGLE USLUGI .PHP - display single uslugi
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
  
  get_header();


  $case_cnt = get_field('case_cnt');
?>

<main class="main" id="scroll">

	<?php 
	get_template_part( 'template-parts/partial', 'services-box-50-50' );
	get_template_part( 'template-parts/partial', 'offer-cnt' );
	get_template_part( 'template-parts/partial', 'offer-services' );
	?>

</main>

<?php
get_footer();