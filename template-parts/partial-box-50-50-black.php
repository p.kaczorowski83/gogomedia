<?php
/**
 * ===============================
 * BOX 50/50 BLACK.PHP - show section box 50/50 with black background
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

?>

<div class="box__50__black">
	<div class="container">	
		<ul>
			<?php while ( have_rows( 'box_50_50_black_loop' ) ) : the_row(); ?>
				<li>
					<div class="box__50__black-img">
						<?php $box_50_50_loop_foto = get_sub_field( 'box_50_50_loop_foto' ); ?>
						<?php $size = 'image610'; ?>
						<?php if ( $box_50_50_loop_foto ) : ?>
							<?php echo wp_get_attachment_image( $box_50_50_loop_foto, $size, false, [
							    'class' => 'lazyload',
							    'loading' => 'lazy',
							    'data-parallax' => '{"y": -50}',
							    'data-src' => wp_get_attachment_image_url( $box_50_50_loop_foto, $size )
							]); ?>
						<?php endif; ?>
					</div>
					<div class="box__50__black-cnt">
						<h3><?php the_sub_field( 'box_50_50_black_loop_title' ); ?></h3>
						<p><?php the_sub_field( 'box_50_50_black_loop_cnt' ); ?></p>
						<?php $box_50_50_black_loop_link = get_sub_field( 'box_50_50_black_loop_link' ); ?>
						<?php if ( $box_50_50_black_loop_link ) : ?>
							<a href="<?php echo esc_url( $box_50_50_black_loop_link['url'] ); ?>" class="red__link"><?php echo esc_html( $box_50_50_black_loop_link['title'] ); ?></a>
						<?php endif; ?>
					</div>	
				</li>
			<?php endwhile; ?>
		</ul>
	</div>
</div>