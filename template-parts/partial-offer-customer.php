<?php
/**
 * ===============================
 * PARTIAL OFFER CUSTOMER .PHP - Box offer for customer
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

?>

<section class="offer__customer">

	<div class="container">	

	<?php if ( have_rows( 'offer_customer' ) ) : ?>
	<ul>
	<?php $i=0; while ( have_rows( 'offer_customer' ) ) : the_row(); ?>
		<li id="col-class-<?php echo ++$i;?>">
			<h3><?php the_sub_field( 'offer_customer_title' ); ?></h3>
			<p><?php the_sub_field( 'offer_customer_cnt' ); ?></p>
			<?php $offer_customer_link = get_sub_field( 'offer_customer_link' ); ?>
			<?php if ( $offer_customer_link ) : ?>
				<a href="<?php echo esc_url( $offer_customer_link); ?>" class="red__link-small"><?php _e('czytaj więcej', 'fastlogic' ); ?></a>
			<?php endif; ?>
		</li>	
	<?php endwhile; ?>
	</ul>
	<?php endif; ?>

	<div class="offer__customer-progress">
		<p id="klient"><?php _e('Odpowiedzialność klienta', 'fastlogic' ); ?></p>

		<div class="offer__customer-ani">			
			<svg width="1241" height="90" viewBox="0 0 1241 90" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 5C0 2.23858 2.23858 0 5 0H1236C1238.76 0 1241 2.23858 1241 5V10.178C1241 12.8689 1238.87 15.0772 1236.18 15.1747L5.18118 59.8121C2.35058 59.9148 0 57.6479 0 54.8154V5Z" fill="#6D6D6D"/><path d="M1241 85C1241 87.7614 1238.76 90 1236 90H5C2.23865 90 0 87.7614 0 85V79.822C0 77.1311 2.12964 74.9228 4.81885 74.8253L1235.82 30.1879C1238.65 30.0852 1241 32.3521 1241 35.1846V85Z" fill="#E1001A"/></svg>
			<section id="red"></section>
			<ul>
				<li id="col1"></li>
				<li id="col2"></li>
				<li id="col3"></li>
			</ul>
		</div>

		<p id="fastlogic"><?php _e('Odpowiedzialność FastLogic', 'fastlogic' ); ?></p>
	</div>


	

	</div>

</section>

