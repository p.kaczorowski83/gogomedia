<?php
/**
 * ===============================
 * PARTIAL WHY US INDUSTRIES.PHP - why-us-industries
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */
$whyus_industries_title = get_field( 'whyus_industries_title' );

$allowed_types = array(
	'span'      => array(),
);

?>

<section class="whyus__industries">
	<div class="container">

		<?php if ($whyus_industries_title): ?>			
			<h2><?php echo wp_kses( __( $whyus_industries_title, 'fastlogic' ), $allowed_types ); ?></h2>
		<?php endif ?>

		<?php if ( have_rows( 'whyus_industries_boxes' ) ) : ?>
			<ul>
				<?php while ( have_rows( 'whyus_industries_boxes' ) ) : the_row(); ?>
					<li>
						<?php $whyus_industries_boxes_icon = get_sub_field( 'whyus_industries_boxes_icon' ); ?>
						<?php $size = 'full'; ?>
						<?php if ( $whyus_industries_boxes_icon ) : ?>
							<?php echo wp_get_attachment_image( $whyus_industries_boxes_icon, $size, false, [
								    'class' => 'lazyload img-fluid',
								    'loading' => 'lazy',
								    'data-src' => wp_get_attachment_image_url( $whyus_industries_boxes_icon, $size )
								]); ?>
						<?php endif; ?>
						<h4><?php the_sub_field( 'whyus_industries_boxes_name' ); ?></h4>
						<?php $whyus_industries_boxes_link = get_sub_field( 'whyus_industries_boxes_link' ); ?>
						<?php if ( $whyus_industries_boxes_link ) : ?>
							<a href="<?php echo esc_url( $whyus_industries_boxes_link); ?>" class="red__link-small">Case study</a>
						<?php endif; ?>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>

		
	</div>

</section>