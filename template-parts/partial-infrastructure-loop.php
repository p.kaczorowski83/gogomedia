<?php
/**
 * ===============================
 * infrastructure-loop.PHP - infrastructure-loop
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */
  $news_cnt = get_field('news_cnt');


  $allowed_types = array(
	'span'      => array(),
   );


?>

<section class="infrastructure">
	
	<?php if ( have_rows( 'infrastructure' ) ) : ?>
	<?php while ( have_rows( 'infrastructure' ) ) : the_row(); ?>
		<h2 class="typo1">
			<?php the_sub_field( 'infrastructure_title' ); ?>
		</h2>

		<!-- ATUTY -->
		<div class="strengths">
			<div class="container">
				<?php if ( have_rows( 'infrastructure_strengths' ) ) : ?>
					<ul>
					<?php while ( have_rows( 'infrastructure_strengths' ) ) : the_row(); ?>
						<li>
							<?php $infrastructure_strengths_img = get_sub_field( 'infrastructure_strengths_img' ); ?>
							<?php $size = 'full'; ?>
							<?php if ( $infrastructure_strengths_img ) : ?>
								<?php echo wp_get_attachment_image( $infrastructure_strengths_img, $size, false, [
									'class' => 'lazyload',
									'loading' => 'lazy',
									'data-src' => wp_get_attachment_image_url( $infrastructure_strengths_img, $size )
								]); ?>
							<?php endif; ?>
							<h3><?php the_sub_field( 'infrastructure_strengths_number' ); ?></h3>
							<p><?php the_sub_field( 'infrastructure_strengths_cnt' ); ?></p>				
						</li>
					<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
		</div>

		<!-- BOXES -->
		<?php if ( have_rows( 'infrastructure_boxes' ) ) : ?>
		<section class="career__boxes">
			<div class="container">

				<div class="box__50__white">
					<ul>
						<?php while ( have_rows( 'infrastructure_boxes' ) ) : the_row(); ?>
							<li>
								<div class="box__50__white-img">
									<?php $infrastructure_boxes_img = get_sub_field( 'infrastructure_boxes_img' ); ?>
									<?php $size = 'image610'; ?>
									<?php if ( $infrastructure_boxes_img ) : ?>
										<?php echo wp_get_attachment_image( $infrastructure_boxes_img, $size, false, [
										    'class' => 'lazyload',
										    'loading' => 'lazy',
										    'data-src' => wp_get_attachment_image_url( $infrastructure_boxes_img, $size )
										]); ?>
									<?php endif; ?>
								</div>
								<div class="box__50__white-cnt">
									<h3><?php the_sub_field( 'infrastructure_boxes_title' ); ?></h3>
									<p><?php the_sub_field( 'infrastructure_boxes_cnt' ); ?></p>
								</div>	
							</li>
						<?php endwhile; ?>
					</ul>
				</div>		
			</div>

		</section>
		<?php endif;?>


		<!-- GALERIA -->
		<?php $infrastructure_gallery_images = get_sub_field( 'infrastructure_gallery' ); ?>
		<?php if ( $infrastructure_gallery_images ) :  ?>
		<div class="container">
			<section class="gallery">
				<?php $infrastructure_gallery_ids = get_sub_field( 'infrastructure_gallery' ); ?>
				<?php $size = 'full'; ?>
				<?php if ( $infrastructure_gallery_ids ) :  ?>
					<?php $i=0; foreach ( $infrastructure_gallery_ids as $infrastructure_gallery_id ): ?>
					<?php $i++;?>
						<figure class="gallery__item gallery__item--<?php echo $i;?>">
							<?php echo wp_get_attachment_image( $infrastructure_gallery_id, $size, false, [
							    'class' => 'lazyload gallery__img',
							    'loading' => 'lazy',
							    'data-src' => wp_get_attachment_image_url( $infrastructure_gallery_id, $size )
								]); ?>
						</figure>
					<?php endforeach; ?>
				<?php endif; ?>
			</section>
		</div>
		<?php endif; ?>

	<?php endwhile; ?>
	<?php endif; ?>

</section>

