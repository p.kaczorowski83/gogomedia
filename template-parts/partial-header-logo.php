<?php
/**
 * ===============================
 * HEADER-LOGO.PHP - logo
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/logo.svg" alt="<?php bloginfo('name'); ?>" class="img-fluid">