<?php
/**
 * ===============================
 * CERTIFICATES.PHP - show section with certificates
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

$case_study_slider_title = get_post_meta(get_the_ID(), 'case_study_slider_title', true );

$allowed_types = array(
	'span'      => array(),
);
?>

<div class="certificates">
	<div class="container">
		<h2><?php echo _e( 'Nasze <span>certyfikaty</span>', 'fastlogic' ) ?></h2>
		<ul>
			<?php if ( have_rows( 'certificates', 'option' ) ) : ?>
			<?php while ( have_rows( 'certificates', 'option' ) ) : the_row(); ?>
				<li>
					<?php $certificates_logo = get_sub_field( 'certificates_logo' ); ?>
					<?php $size = 'full'; ?>
					<?php if ( $certificates_logo ) : ?>
						<?php echo wp_get_attachment_image($certificates_logo, $size, false, [
							'class' => 'lazyload',
							'loading' => 'lazy',
							'data-src' => wp_get_attachment_image_url($certificates_logo, $size )
						]); ?>
					<?php endif; ?>
					<div class="certificates__txt">
						<div class="row">
							<?php the_sub_field( 'certificates_name' ); ?>
							<?php $certificates_pdf = get_sub_field( 'certificates_pdf' ); ?>
							<?php if ( $certificates_pdf ) : ?>
								<a href="<?php echo esc_url( $certificates_pdf['url'] ); ?>" target="_blank"><?php echo _e('Zobacz certyfikat', 'fastlogic');?></a>
							<?php endif; ?>
						</div>	
					</div>
				</li>
			<?php endwhile; ?>
		<?php endif; ?>
		</ul>
	</div>
</div>