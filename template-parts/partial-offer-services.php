<?php
/**
 * ===============================
 * PARTIAL OFFER CUSTOMER .PHP - Box offer for customer
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

?>

<div class="offer__services">
	<div class="container">
		<h2><?php _e('Struktura i szczegóły <span>naszych usług</span>', 'fastlogic' ); ?></h2>
	</div>
		<ul>
			<?php
			global $post;
	        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	        $args = array(
	        'posts_per_page' => 10,
	        'paged' => $paged,
	        'post_type' => 'uslugi',
	        'post__not_in' => array(get_the_ID()),

		    );
		    $loop = new WP_Query( $args );
		    if ( $loop->have_posts() ) {
		    while ( $loop->have_posts() ) : $loop->the_post();
		    	$services_lead = get_field('services_lead');
		    ?>
		    <li>
		    	<a href="<?php the_permalink();?>" title="<?php the_title();?>">
		    		<div class="offer__services-apla-1"></div>	
		    		<div class="offer__services-apla-2"></div>
		    		<div class="offer__services-cnt">
		    			<div class="container">	
			    			<h3><?php the_title();?></h3>
			    			<?php if($services_lead):?><p><?php echo $services_lead;?></p><?php endif;?>
			    			<span><?php _e('Czytaj więcej', 'fastlogic' ); ?></span>
		    			</div>		
		    		</div>
		    		<?php $services_img = get_field( 'services_img' ); ?>
		    		<?php $size = 'imageServices'; ?>
		    		<?php if ( $services_img ) : ?>
		    			<?php echo wp_get_attachment_image( $services_img, $size, false, [
						'class' => 'lazyload img-fluid',
						'loading' => 'lazy',
						'data-src' => wp_get_attachment_image_url( $services_img, $size )
					]); ?>
		    		<?php endif; ?>
		    	</a>
		    </li>
		    <?php endwhile;?>
		</ul>

		<?php
	    }
	    wp_reset_postdata();
	    ?>	
		</ul>	
</div>	

