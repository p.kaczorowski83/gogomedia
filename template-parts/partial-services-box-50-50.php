<?php
/**
 * ===============================
 * BOX LINE.PHP - show section box 50/50 with line center
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

$services_title = get_field('services_title');
$services_title_lead = get_field('services_title_lead');

$allowed_types = array(
	'span'      => array(),
);
?>

<div class="box__line">
	<div class="container">
		<h2 <?php if ($services_title_lead): ?>class="typo1"<?php endif;?>>
			<?php echo _e( $services_title, 'fastlogic' ) ?>
		</h2>
		<?php if ($services_title_lead): ?>
			<p class="text-center"><?php echo $services_title_lead ?></p>
		<?php endif ?>

		<?php if ( have_rows( 'services_title_loop' ) ) : ?>
			<ul <?php if ($services_title_lead): ?>class="marginTop"<?php endif;?>>
				<?php while ( have_rows( 'services_title_loop' ) ) : the_row(); ?>
					<li>
						<div class="box__line-col box__line-foto">
							<?php $box_50_50_loop_icon = get_sub_field( 'box_50_50_loop_icon' ); ?>
							<?php $size = 'full'; ?>
							<?php if ( $box_50_50_loop_icon ) : ?>
								<?php echo wp_get_attachment_image( $box_50_50_loop_icon, $size, false, [
								    'class' => 'lazyload',
								    'loading' => 'lazy',
								    'data-parallax' => '{"y": -50}',
								    'data-src' => wp_get_attachment_image_url( $box_50_50_loop_icon, $size )
								]); ?>
							<?php endif; ?>
						</div>
						<div class="box__line-col box__line-cnt">
							<h3><?php the_sub_field( 'box_50_50_loop_title' ); ?></h3>
							<p><?php the_sub_field( 'box_50_50_loop_cnt' ); ?></p>
						</div>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>
	</div>
</div>