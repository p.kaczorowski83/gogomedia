<?php
/**
 * ===============================
 * STRENGTHS.PHP - show strengths section
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

$strengths_title = get_post_meta(get_the_ID(), 'strengths_title', true );

$allowed_types = array(
	'span'      => array(),
);
?>

<div class="strengths">
	<div class="container">
		<h2><?php echo wp_kses( __( $strengths_title, 'fastlogic' ), $allowed_types ); ?></h2>

		<?php if ( have_rows( 'strengths_loop' ) ) : ?>
			<ul>
			<?php while ( have_rows( 'strengths_loop' ) ) : the_row(); ?>
				<li>
					<?php $strengths_loop_icon = get_sub_field( 'strengths_loop_icon' ); ?>
					<?php $size = 'full'; ?>
					<?php if ( $strengths_loop_icon ) : ?>
						<?php echo wp_get_attachment_image( $strengths_loop_icon, $size, false, [
							'class' => 'lazyload',
							'loading' => 'lazy',
							'data-parallax' => '{"y": -20}',
							'data-src' => wp_get_attachment_image_url( $strengths_loop_icon, $size )
						]); ?>
					<?php endif; ?>
					<h3><?php the_sub_field( 'strengths_loop_number' ); ?></h3>
					<p><?php the_sub_field( 'strengths_loop_txt' ); ?></p>				
				</li>
			<?php endwhile; ?>
			</ul>
		<?php endif; ?>
	</div>
</div>