<?php
/**
 * ===============================
 * HEADER HERO.PHP - hero
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

$hero_title = get_post_meta(get_the_ID(), 'hero_title', true );
$hero_txt = get_field('hero_cnt');

$hero_img = get_field( 'hero_img' );
$footer_header_work = get_option('options_footer_header_work');

$header_case_title = get_option('options_header_case_title');
$header_case_text = get_option('options_header_case_text');

$hero_video = get_field( 'hero_video' ); 

$hero_img_small = get_field('hero_img_small');
$size = 'image1920';
$size_small = 'imageHeo';

$allowed_types = array(
	'br'     => array(),
	'strong' => array(),
	'p'      => array(),
	'span'      => array(),
);
?>

<?php if ( is_single() && 'post' == get_post_type() or is_tax('cat-case-study') ): ?>
<?php else: ?>	

<?php if( is_post_type_archive('case-study') ) :?>

<div class="hero">
	<?php $header_case = get_field( 'header_case', 'option' ); ?>
		<?php $size = 'full'; ?>
		<?php if ( $header_case ) : ?>
			<?php echo wp_get_attachment_image( $header_case, $size ); ?>
		<?php endif; ?>
	<div class="hero__cnt">		
		<h1><?php echo $header_case_title;?></h1>
		<?php if ($header_case_text) :?><p><?php echo wp_kses( __( $header_case_text, 'fastlogic' ), $allowed_types ); ?></p><?php endif;?>
	</div>

	<!-- ARROW -->
	<a href="#scroll" class="hero__arrow"></a>	


</div>

<?php else :?>

<?php if ( $hero_img or $footer_header_work ) : ?>
<div class="<?php if ($hero_img): ?>hero <?php else :?>hero__small<?php endif ?>">

	<?php if ( $hero_video ) : ?>
		<video playsinline="" autoplay="" muted="" loop="">
			<source src="<?php echo esc_url( $hero_video['url'] ); ?>" type="video/mp4">
		</video>

	<?php elseif ( $hero_img ) : ?>
		<?php echo wp_get_attachment_image( $hero_img, $size ); ?>

	<?php elseif ($hero_img_small): ?>
		<?php echo wp_get_attachment_image( $hero_img_small, $size_small ); ?>

	<?php else :?>
		<?php echo wp_get_attachment_image( $footer_header_work, $size_small ); ?>
	<?php endif ?>

	<div class="hero__cnt">
		<h1><?php if($hero_title):?><?php echo wp_kses( __( $hero_title, 'fastlogic' ), $allowed_types ); ?><?php else :?><?php the_title();?><?php endif;?></h1>
		<?php if ($hero_txt) :?><p><?php echo wp_kses( __( $hero_txt, 'fastlogic' ), $allowed_types ); ?></p><?php endif;?>

		<?php if ( have_rows( 'hero_btn' ) ) : ?>
			<ul>
			<?php while ( have_rows( 'hero_btn' ) ) : the_row(); ?>
				<?php $hero_btn_link = get_sub_field( 'hero_btn_link' ); ?>
				<?php if ( $hero_btn_link ) : ?>
					<li>
						<a href="<?php echo esc_url( $hero_btn_link['url'] ); ?>"><?php echo esc_html( $hero_btn_link['title'] ); ?></a>
					</li>
				<?php endif; ?>
			<?php endwhile; ?>
			</ul>
		<?php endif; ?>
	</div>

	<?php if ($hero_img): ?>
	<!-- ARROW -->
	<a href="#scroll" class="hero__arrow"></a>	
	<?php endif;?>

</div>
<?php endif;?>
<?php endif ?>
<?php endif;?>

