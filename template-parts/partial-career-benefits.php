<?php
/**
 * ===============================
 * PARTIAL CAREER BENEFITS.PHP - career-benefits
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */
$career_benefits_title = get_field( 'career_benefits_title' );

$allowed_types = array(
	'span'      => array(),
);

?>

<section class="career__benefits">
	<div class="container">

		<?php if ($career_benefits_title): ?>			
			<h2><?php echo wp_kses( __( $career_benefits_title, 'fastlogic' ), $allowed_types ); ?></h2>
		<?php endif ?>

		<?php if ( have_rows( 'career_benefits' ) ) : ?>
			<ul class="career__loop">
				<?php while ( have_rows( 'career_benefits' ) ) : the_row(); ?>
					<li>
						<?php $career_benefits_img = get_sub_field( 'career_benefits_img' ); ?>
						<?php $size = 'full'; ?>
						<?php if ( $career_benefits_img ) : ?>
							<?php echo wp_get_attachment_image( $career_benefits_img, $size, false, [
								    'class' => 'lazyload img-fluid',
								    'loading' => 'lazy',
								    'data-src' => wp_get_attachment_image_url( $career_benefits_img, $size )
								]); ?>
						<?php endif; ?>
						<h4><?php the_sub_field( 'career_benefits_txt' ); ?></h4>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>

		
	</div>

</section>