<?php
/**
 * ===============================
 * PARTIAL CAREER BOXES.PHP - career-boxes
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

?>

<section class="career__boxes">
	<div class="container">

		<div class="box__50__white">
			<ul>
				<?php while ( have_rows( 'career_boxes' ) ) : the_row(); ?>
					<li>
						<div class="box__50__white-img">
							<?php $career_boxes_img = get_sub_field( 'career_boxes_img' ); ?>
							<?php $size = 'image610'; ?>
							<?php if ( $career_boxes_img ) : ?>
								<?php echo wp_get_attachment_image( $career_boxes_img, $size, false, [
								    'class' => 'lazyload',
								    'loading' => 'lazy',
								    'data-src' => wp_get_attachment_image_url( $career_boxes_img, $size )
								]); ?>
							<?php endif; ?>
						</div>
						<div class="box__50__white-cnt">
							<h3><?php the_sub_field( 'career_boxes_title' ); ?></h3>
							<p><?php the_sub_field( 'career_boxes_cnt' ); ?></p>
						</div>	
					</li>
				<?php endwhile; ?>
			</ul>
		</div>		
	</div>

</section>