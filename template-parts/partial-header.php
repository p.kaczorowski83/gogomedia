<?php
/**
 * ===============================
 * HEADER.PHP - main header file
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<header class="navbar__fixed <?php if ( is_single() && 'post' == get_post_type() or is_tax('cat-case-study') ): ?>navbar__fixed-black<?php endif;?>">	
	<div class="navbar__fixed-cnt">

		<!-- LOGO -->
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar__fixed-logo">
			<?php 
			get_template_part( 'template-parts/partial', 'header-logo' ); 
			?>	
		</a>
		
		<!-- MENU  -->
		<nav>
			<?php
			wp_nav_menu(
				array(
				'theme_location' => 'main-menu',
				'container'      => '',
				'menu_class'     => 'navbar__fixed-nav',
				'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
				'walker'         => new WP_Bootstrap_Navwalker(),
				)
			);
			?>		
		</nav> 

		<!-- LANG -->
		<div class="navbar__fixed-lang">
			<?php echo do_action('wpml_add_language_selector');?>
		</div>
		
		<!-- HAMBURGER -->
		<div class="hamburger">
			<div class="line line1"></div>
			<div class="line line2"></div>
			<div class="line line3"></div>
		</div>
		
	</div><!-- end .navbar__fixed-cnt -->
	
</header>