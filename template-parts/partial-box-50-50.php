<?php
/**
 * ===============================
 * BOX LINE.PHP - show section box 50/50 with line center
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

$box_50_50_title = get_field('box_50_50_title');

$allowed_types = array(
	'span'      => array(),
);
?>

<div class="box__line-50">
	<div class="container">
		<h2><?php echo _e( $box_50_50_title, 'fastlogic' ) ?></h2>

		<?php if ( have_rows( 'box_50_50_loop' ) ) : ?>
			<ul>
				<?php while ( have_rows( 'box_50_50_loop' ) ) : the_row(); ?>
					<li>
						<div class="box__line-col box__line-foto">
							<?php $box_50_50_loop_icon = get_sub_field( 'box_50_50_loop_icon' ); ?>
							<?php $size = 'full'; ?>
							<?php if ( $box_50_50_loop_icon ) : ?>
								<?php echo wp_get_attachment_image( $box_50_50_loop_icon, $size, false, [
								    'class' => 'lazyload',
								    'loading' => 'lazy',
								    'data-parallax' => '{"y": -50}',
								    'data-src' => wp_get_attachment_image_url( $box_50_50_loop_icon, $size )
								]); ?>
							<?php endif; ?>
						</div>
						<div class="box__line-col box__line-cnt">
							<h3><?php the_sub_field( 'box_50_50_loop_title' ); ?></h3>
							<p><?php the_sub_field( 'box_50_50_loop_cnt' ); ?></p>
						</div>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>
	</div>
</div>