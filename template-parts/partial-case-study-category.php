<?php
/**
 * ===============================
 * PARTIAL CASE STUDY CATEGORY.PHP - case study list category
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

?>

<div class="container">	
	<ul class="casestudy__menu <?php if (is_tax('cat-case-study')): ?>casestudy__menu-tax<?php endif ?>">
		<li <?php if( is_post_type_archive('case-study') ) :?>class="current-cat"<?php endif;?>>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>case-study/" title="<?php echo _e('Wszystkie', 'fastlogic')?>"><?php echo _e('Wszystkie', 'fastlogic')?></a>
		</li>
	    <?php wp_list_categories( array(
		    'orderby' => 'name',
		    'title_li' => '',
		    'taxonomy' => 'cat-case-study'
		) ); ?> 
	</ul>
</div>	