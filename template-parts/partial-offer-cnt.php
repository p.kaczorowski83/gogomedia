<?php
/**
 * ===============================
 * PARTIAL OFFER CNT.PHP
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

$services_what = get_post_meta(get_the_ID(), 'services_what', true );
$services_what_analysis = get_post_meta(get_the_ID(), 'services_what_analysis', true );

$allowed_types = array(
	'span'      => array(),
);

?>

<section class="offer__cnt">

	<div class="container">
		
		<?php if ($services_what): ?>			
			<h2><?php echo wp_kses( __( $services_what, 'fastlogic' ), $allowed_types ); ?></h2>
		<?php endif ?>

		<?php if ( have_rows( 'services_what_loop' ) ) : ?>
			<ul class="offer__cnt-li">
			<?php while ( have_rows( 'services_what_loop' ) ) : the_row(); ?>
				<li><?php the_sub_field( 'services_what_loop_cnt' ); ?></li>
			<?php endwhile; ?>
			</ul>
		<?php else : ?>
			<?php // no rows found ?>
		<?php endif; ?>

		<?php if ($services_what_analysis): ?>			
			<h2 class="analysis"><?php echo wp_kses( __( $services_what_analysis, 'fastlogic' ), $allowed_types ); ?></h2>
		<?php endif ?>

		<?php the_field( 'services_what_analysis_cnt' ); ?>


	</div>

	

</section>

