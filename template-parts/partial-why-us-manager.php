<?php
/**
 * ===============================
 * PARTIAL WHY US MANAGER.PHP - why-us-manager
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

$whyus_title = get_post_meta(get_the_ID(), 'whyus_title', true );

$allowed_types = array(
	'span'      => array(),
);
?>

<section>
	<div class="container">
		
		<?php if ($whyus_title): ?>			
			<h2><?php echo wp_kses( __( $whyus_title, 'fastlogic' ), $allowed_types ); ?></h2>
		<?php endif ?>

		<?php if ( have_rows( 'whyus_team' ) ) : ?>
			<ul class="whyus__manager">
				<?php while ( have_rows( 'whyus_team' ) ) : the_row(); ?>
					<li>
						<div class="whyus__manager-foto">
							<?php $whyus_team_img = get_sub_field( 'whyus_team_img' ); ?>
							<?php $size = 'image400'; ?>
							<?php if ( $whyus_team_img ) : ?>
								<?php echo wp_get_attachment_image( $whyus_team_img, $size, false, [
								    'class' => 'lazyload img-fluid',
								    'loading' => 'lazy',
								    'data-src' => wp_get_attachment_image_url( $whyus_team_img, $size )
								]); ?>
							<?php endif; ?>
						</div>
						<div class="whyus__manager-cnt">
							<?php 
							$whyus_team_name = get_sub_field('whyus_team_name');
							$whyus_team_position = get_sub_field('whyus_team_position');
							$whyus_team_cnt = get_sub_field('whyus_team_cnt');
							$whyus_team_mail = get_sub_field('whyus_team_mail');
							$whyus_team_linkedin = get_sub_field('whyus_team_linkedin'); ?>
							<?php if ($whyus_team_mail): ?>
								<a href="mailto:<?php echo $whyus_team_mail?>" class="whyus__manager-mail <?php if (!$whyus_team_linkedin): ?>whyus__manager-nolinedin<?php endif;?>"></a>
							<?php endif ?>
							<?php if ($whyus_team_linkedin): ?>
								<a href="<?php echo $whyus_team_linkedin?>" class="whyus__manager-linkedin"></a>
							<?php endif ?>							
							<h3><?php echo $whyus_team_name?></h3>
							<?php if ($whyus_team_position): ?><span><?php echo $whyus_team_position?></span><?php endif ?>
							<p><?php echo$whyus_team_cnt;?></p>
						</div>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>

	</div>

</section>

