<?php
/**
 * ===============================
 * FOOTER LOGO .PHP - footer logo banner section
 * ===============================
 *
 * @package fastlogic
 * @since 1.0.0
 * @version 1.0.0
 */

$footer_email = get_option('options_footer_email');
$footer_phone = get_option('options_footer_phone');
$phone = str_replace(' ', '', get_option("options_footer_phone"));
$footer_linkedin = get_option('options_footer_linkedin');

$allowed_types = array(
    'span'     => array(),
);

?>

<div class="footer__logo">
    <div class="container">
        <?php  $attachment_id = get_field( 'footer_logo', 'option' );
        $size = 'full';?>
		<?php if ($attachment_id ) : ?>
			<?php echo wp_get_attachment_image( $attachment_id, $size, false, [
			    'class' => 'lazyload footer-logo',
			    'loading' => 'lazy',
			    'data-src' => wp_get_attachment_image_url( $attachment_id, $size )
			]); ?>
		<?php endif; ?>

		<ul class="footer__logo-tel">
			<li><a href="tel:<?php echo $phone;?>"><?php esc_html_e( $footer_phone, 'fastlogic' ); ?></a></li>
			<li><a href="mailto:<?php esc_html_e( $footer_email, 'fastlogic' ); ?>"><?php esc_html_e( $footer_email, 'fastlogic' ); ?></a></li>
		</ul>

		<ul class="footer__logo-adress">
			<li><strong><?php _e('Adres', 'fastlogic' ); ?></strong> <?php the_field( 'footer_adress', 'option' ); ?></li>
			<li><strong><?php _e('VAT', 'fastlogic' ); ?></strong> <?php the_field( 'footer_vat', 'option' ); ?></li>
			<li><strong><?php _e('REGON', 'fastlogic' ); ?></strong> <?php the_field( 'footer_regon', 'option' ); ?></li>
			<li><strong><?php _e('KRS', 'fastlogic' ); ?></strong> <?php the_field( 'footer_kres', 'option' ); ?></li>
		</ul>

		<?php if ($footer_linkedin): ?>
			<div class="footer__logo-linkeind">
				<a href="<?php echo $footer_linkedin?>" target="_blank">
					<img loading="lazy" class="lazyload" data-src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-footer-linkeind.svg" alt="" />
				</a>
			</div>			
		<?php endif ?>
	</div><!-- ./ container -->
</div>