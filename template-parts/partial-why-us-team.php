<?php
/**
 * ===============================
 * PARTIAL WHY US TEAM.PHP - why-us-leaders
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

$whyus_leaders_title = get_post_meta(get_the_ID(), 'whyus_leaders_title', true );
$whyus_leaders_lead = get_field('whyus_leaders_lead');

$allowed_types = array(
	'span'      => array(),
);
?>

<section class="whyus__leaders-bg">
	<div class="container">
		
		<?php if ($whyus_leaders_title): ?>			
			<h2><?php echo wp_kses( __( $whyus_leaders_title, 'fastlogic' ), $allowed_types ); ?></h2>
		<?php endif ?>

		<?php if ($whyus_leaders_lead): ?>
			<p><?php echo $whyus_leaders_lead;?></p>
		<?php endif ?>

		<?php if ( have_rows( 'whyus_leaders' ) ) : ?>
			<ul class="whyus__leaders">
				<?php while ( have_rows( 'whyus_leaders' ) ) : the_row(); ?>
					<li>
						<div class="whyus__leaders-foto">
							<?php $whyus_leaders_img = get_sub_field( 'whyus_leaders_img' ); ?>
							<?php $size = 'image220'; ?>
							<?php if ( $whyus_leaders_img ) : ?>
								<?php echo wp_get_attachment_image( $whyus_leaders_img, $size, false, [
								    'class' => 'lazyload img-fluid',
								    'loading' => 'lazy',
								    'data-src' => wp_get_attachment_image_url( $whyus_leaders_img, $size )
								]); ?>
							<?php endif; ?>
						</div>
						<div class="whyus__leaders-cnt">
							<p><?php the_sub_field( 'whyus_leaders_scientific' ); ?></p>
							<h4><?php the_sub_field( 'whyus_leaders_name' ); ?></h4>
							<p><?php the_sub_field( 'whyus_leaders_positions' ); ?></p>
						</div>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>

	</div>

</section>