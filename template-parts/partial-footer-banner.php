<?php
/**
 * ===============================
 * FOOTER BANNER .PHP - footer red banner section
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$footer_banner = get_option('options_footer_banner');
$footer_banner_subtitle = get_option('options_footer_banner_subtitle');
$footer_banner_button = get_option('options_footer_banner_button');

$allowed_types = array(
    'span'     => array(),
);

?>

<div class="footer__banner">
    <div class="container">
        <p><?php echo wp_kses( __( $footer_banner, 'fastlogic' ), $allowed_types ); ?></p>
        <span><?php echo wp_kses( __( $footer_banner_subtitle, 'fastlogic' ), $allowed_types ); ?></span>
        <?php if ( $footer_banner_button ) : ?>
            <a href="<?php echo esc_url( $footer_banner_button['url'] ); ?>"><?php echo esc_html( $footer_banner_button['title'] ); ?></a>
        <?php endif; ?>
    </div>
</div>

