<?php
/**
 * ===============================
 * PARTIAL Gallery.PHP - gallery grid
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

?>

<div class="container">
	<section class="gallery">
	<?php $gallery_ids = get_field( 'gallery' ); ?>
		<?php $size = 'full'; ?>
		<?php if ( $gallery_ids ) :  ?>
			<?php $i=0; foreach ( $gallery_ids as $gallery_id ): ?>
			<?php $i++;?>
				<figure class="gallery__item gallery__item--<?php echo $i;?>">
					<?php echo wp_get_attachment_image( $gallery_id, $size, false, [
					    'class' => 'lazyload gallery__img',
					    'loading' => 'lazy',
					    'data-src' => wp_get_attachment_image_url( $gallery_id, $size )
						]); ?>
				</figure>
			<?php endforeach; ?>
		<?php endif; ?>
	</section>
</div>