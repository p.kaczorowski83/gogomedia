<?php
/**
 * ===============================
 * SINGLE CASE STUDY .PHP - display single case study
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
  
  get_header();


  $case_cnt = get_field('case_cnt');
?>

<main class="main" id="scroll">

	<div class="container">
		<section class="casestudy__single">
			<?php echo $case_cnt ?>
		</section>
	</div>

	<?php 
	get_template_part('template-parts/partial', 'case-study-single-slider');
	?>

</main>

<?php
get_footer();