<?php
/**
 * ===============================
 * FOOTER.PHP - footer faile
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */
?>
<footer id="footer">

	<?php 
	get_template_part('template-parts/partial', 'footer-banner');
    get_template_part('template-parts/partial', 'footer-logo');
    get_template_part('template-parts/partial', 'footer-menu');
    get_template_part('template-parts/partial', 'footer-ue');
    get_template_part('template-parts/partial', 'footer-copy');
	?>
   
</footer><!-- /#footer -->
</div>

<?php wp_footer(); ?>
<script>
WebFontConfig = {
    google: { families: ['Dosis:wght@400;500;700:latin-ext&display=swap'] }
};

(function() {
    var wf = document.createElement('script');
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
})();
</script>
</body>

</html>