<?php
/**
 * ===============================
 * HEADER.PHP - main header file
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">	
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<?php wp_head(); ?>

	<!-- Google Analytics -->
	<!-- End Google Analytics -->

</head>

<body>

<?php wp_body_open(); ?>

<?php 
get_template_part( 'template-parts/partial', 'mobile-menu' );
get_template_part( 'template-parts/partial', 'header' );  
?>

<div class="load">

<?php get_template_part( 'template-parts/partial', 'header-hero' ); ?>

